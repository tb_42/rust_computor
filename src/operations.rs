use crate::types;
use crate::complex;
use crate::matrix;

pub fn product(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let first_type = types::get_type(x, vars);
    let second_type = types::get_type(y, vars);

    if first_type == "pure_im" && second_type == "complex" {
        return complex::product_ic(x, y, vars);
    } else if first_type == "complex" && second_type == "pure_im" {
        return complex::product_ic(y, x, vars);
    } else if first_type == "pure_im" && second_type == "pure_im" {
        return complex::product_ii(y, x);
    } else if first_type == "rational" && second_type == "pure_im" {
        return complex::product_ri(x, y);
    } else if first_type == "pure_im" && second_type == "rational" {
        return complex::product_ri(y, x);
    } else if first_type == "complex" && second_type == "complex" {
        return complex::product_cc(x, y, vars);
    } else if first_type == "rational" && second_type == "complex" {
        return complex::product_rc(x, y, vars);
    } else if first_type == "complex" && second_type == "rational" {
        return complex::product_rc(y, x, vars);
    } else if first_type == "rational" && second_type == "rational" {
        return (x.parse::<f64>().unwrap() * y.parse::<f64>().unwrap()).to_string();
    } else if first_type == "rational" && second_type == "matrix" {
        return matrix::product_r(x, y, vars);
    } else if first_type == "matrix" && second_type == "rational" {
        return matrix::product_r(y, x, vars);
    } else if first_type == "matrix" && second_type == "matrix" {
        return matrix::product(x, y, vars);
    } else {
        return "ERROR".to_string();
    }
}

pub fn m_product(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let first_type = types::get_type(x, vars);
    let second_type = types::get_type(y, vars);

    if first_type == "matrix" && second_type == "matrix" {
        return matrix::m_product(x, y, vars);
    } else {
        return "ERROR".to_string();
    }
}

pub fn power(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let second_type = types::get_type(y, vars);
    let mut res = x.clone();
    
    if second_type == "rational" {

        let pow = y.parse::<f64>().unwrap();
        if pow.fract() != 0.0 || pow < 0f64 {
            return "ERROR".to_string();
        } else if pow == 0f64 {
            return "1".to_string();
        }
        let num = (pow - 1f64) as usize;
        for _i in 0..num {
            res = product(&res, x, vars);
            if res == "ERROR" {
                return "ERROR".to_string();
            }
        }
    }
    return res;
}

pub fn divide(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let first_type = types::get_type(x, vars);
    let second_type = types::get_type(y, vars);

    if first_type == "pure_im" && second_type == "complex" {
        return complex::divide_ic(x, y, vars);
    } else if first_type == "complex" && second_type == "pure_im" {
        return complex::divide_ci(y, x, vars);
    } else if first_type == "pure_im" && second_type == "pure_im" {
        return complex::divide_ii(y, x);
    } else if first_type == "rational" && second_type == "pure_im" {
        return complex::divide_ri(x, y);
    } else if first_type == "pure_im" && second_type == "rational" {
        return complex::divide_ir(y, x);
    } else if first_type == "complex" && second_type == "complex" {
        return complex::divide_cc(x, y, vars);
    } else if first_type == "rational" && second_type == "complex" {
        return complex::divide_rc(x, y, vars);
    } else if first_type == "complex" && second_type == "rational" {
        return complex::divide_cr(y, x, vars);
    } else if first_type == "rational" && second_type == "rational" {
        return (x.parse::<f64>().unwrap() / y.parse::<f64>().unwrap()).to_string();
    } else {
        return "ERROR".to_string();
    }
}

pub fn modulo(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let first_type = types::get_type(x, vars);
    let second_type = types::get_type(y, vars);

    if first_type == "rational" && second_type == "rational" {
        let new_x = x.parse::<f64>().unwrap();
        let new_y = y.parse::<f64>().unwrap();

        if new_x.fract() != 0.0 || new_y.fract() != 0.0 {
            return "ERROR".to_string();
        } else {
            return sub_modulo(new_x as i32, new_y as i32).to_string();
        }
    } else {
        return "ERROR".to_string();
    }
}

pub fn add(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let first_type = types::get_type(x, vars);
    let second_type = types::get_type(y, vars);

    if first_type == "pure_im" && second_type == "complex" {
        return complex::add_ic(x, y, vars);
    } else if first_type == "complex" && second_type == "pure_im" {
        return complex::add_ic(y, x, vars);
    } else if first_type == "pure_im" && second_type == "pure_im" {
        return complex::add_ii(y, x);
    } else if first_type == "pure_im" && second_type == "rational" {
        return complex::add_ir(x, y, vars);
    } else if first_type == "rational" && second_type == "pure_im" {
        return complex::add_ir(y, x, vars);
    } else if first_type == "complex" && second_type == "complex" {
        return complex::add_cc(x, y, vars);
    } else if first_type == "rational" && second_type == "complex" {
        return complex::add_rc(x, y, vars);
    } else if first_type == "complex" && second_type == "rational" {
        return complex::add_rc(y, x, vars);
    } else if first_type == "rational" && second_type == "rational" {
        return (x.parse::<f64>().unwrap() + y.parse::<f64>().unwrap()).to_string();
    } else if first_type == "matrix" && second_type == "matrix" {
        return matrix::add(x, y, vars);
    } else {
        return "ERROR".to_string();
    }
}

pub fn substract(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let first_type = types::get_type(x, vars);
    let second_type = types::get_type(y, vars);

    if first_type == "pure_im" && second_type == "complex" {
        return complex::substract_ic(x, y, vars);
    } else if first_type == "complex" && second_type == "pure_im" {
        return complex::substract_ci(x, y, vars);
    } else if first_type == "pure_im" && second_type == "pure_im" {
        return complex::substract_ii(y, x);
    } else if first_type == "pure_im" && second_type == "rational" {
        return complex::substract_ir(x, y, vars);
    } else if first_type == "rational" && second_type == "pure_im" {
        return complex::substract_ri(x, y, vars);
    } else if first_type == "complex" && second_type == "complex" {
        return complex::substract_cc(x, y, vars);
    } else if first_type == "rational" && second_type == "complex" {
        return complex::substract_rc(x, y, vars);
    } else if first_type == "complex" && second_type == "rational" {
        return complex::substract_cr(x, y, vars);
    } else if first_type == "rational" && second_type == "rational" {
        return (x.parse::<f64>().unwrap() - y.parse::<f64>().unwrap()).to_string();
    } else if first_type == "matrix" && second_type == "matrix" {
        return matrix::substract(x, y, vars);
    } else {
        return "ERROR".to_string();
    }
}

fn _pgcd(x: i32, y: i32) -> i32 {

    if y == 0 {
        return x
    }
    return _pgcd(y, sub_modulo(x, y));
}

fn sub_modulo(x: i32, y: i32) -> i32 {

    let mut a = y;

    if x < y {
        return x;
    } loop {
        a += y;
        if a > x {
            a -= y;
            return x - a;
        } else if a == x {
            return 0;
        }
    }
}
