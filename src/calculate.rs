use crate::types;
use crate::complex;
use crate::assign;

pub fn calculate_result(vect: &mut Vec<String>, vars: &mut types::MegaStruct) {

    if !assign::solve_parenthesis(vect, vars, false) {
        println!("ERROR");
        types::reset_temps(vars);
        return ;
    }
    // println!("{:?}", vect);
    if types::get_type(&vect[0], vars) == "complex" {
        complex::assign(&vect[0], &vect[0], vars, true);
    } else if types::get_type(&vect[0], vars) == "rational" {
        println!("{}", vect[0].parse::<f64>().unwrap());
    } else if types::get_type(&vect[0], vars) == "matrix" {
        for i in 0..vars.matrix[&vect[0]].dimension[0] {
            println!("{:?}", vars.matrix[&vect[0]].value[i as usize]);
        }
    } else {
        println!("ERROR");
        types::reset_temps(vars);
        return ;
    }
    types::reset_temps(vars);
}