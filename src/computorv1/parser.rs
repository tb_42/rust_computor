use crate::computorv1::solver;
use crate::types;
use crate::function;
use crate::syntax;

pub fn parser(text: &String, args: &mut types::MegaStruct) {

    let mut result = Vec::new();
    let mut vars = Vec::new();
    let mut last = 0;

    for (index, matched) in text.match_indices(|c: char| !((c.is_alphanumeric()) || c == '.' || c == '^')) {
        if last != index {
            result.push(&text[last..index]);
        }
        result.push(matched);
        last = index + matched.len();
    }
    if last < text.len() {
        result.push(&text[last..]);
    }
    result.retain(|x| *x != " " && *x != "?" && *x != "\n");
    for i in result {
        vars.push(i.to_string());
    }
    function::expand(&mut vars, args, false);
    syntax::expand_rationals(&mut vars, args, false);
    syntax::min_product_reduce(&mut vars, args);
    check_reduced_x(&mut vars);
    contract_power(&mut vars);
    invert_x(&mut vars, 0);
    println!("{:?}", vars);
    if syntax_error(&vars) == true {
        println!("here");
        println!("Syntax Error");
    } else {
        solver::pol_values(&vars);
    }
}

fn syntax_error(vect: &Vec<String>) -> bool {

    let mut equal: bool = false;

    for (i, v) in vect.iter().enumerate() {
        match v.as_str() {
            "+" | "-" => {
                if i + 1 > vect.len() - 1 || vect[i + 1] == "+" || vect[i + 1] == "-" 
                || vect[i + 1] == "=" || vect[i + 1] == "*" {
                    println!("1");
                    return true;
                }
            },
            "=" => {
                if i == 0 || i + 1 > vect.len() - 1 || i + 1 > vect.len() - 1
                || equal || vect[i + 1] == "+" {
                    return true;
                }
                equal = true;
            },
            "*" => {
                if i + 1 > vect.len() - 1 || (&vect[i + 1][0..1] != "X" && &vect[i + 1][0..1] != "x") {
                    println!("2");
                    return true;
                }
            },
            "X" | "x" | "^" | "(" | ")" => {
                continue
            },
            _ => {
                if v.len() > 2 && (&v[0..2] == "X^" || &v[0..2] == "x^") {
                    match &v[2..].parse::<i8>() {
                        Ok(_s) => continue,
                        Err(_e) => {println!("3");return true;},
                    }
                }
                else {
                    match v.parse::<f64>() {
                        Ok(_s) => continue,
                        Err(_e) => {println!("4");return true;},
                    }
                }
            },
        }
    }
    if equal {
        return false;
    } else {
        println!("here");
        return true;
    }
}

pub fn check_reduced_x(vect: &mut Vec<String>) {

    let mut num_mode: bool = false;
    let mut num_literal: String = String::new();
    let rest_literal: String;
    let mut index = 0;
    let mut outloop: bool = false;

    for (i, v) in vect.iter().enumerate() {
        for c in v.chars() {
            match c {
                '0'..='9' | '.' => {
                    if !num_mode {
                        num_mode = true;
                    }
                    num_literal += &c.to_string();
                }
                'a'..='z' | 'A'..='Z' => {
                    if num_mode {
                        index = i;
                        outloop = true;
                        break ;
                    }
                }
                _ => {
                    num_mode = false;
                    num_literal = "".to_string();
                    continue;
                }
            }
        }
        if outloop {
            break ;
        }
    }
    if outloop {
        rest_literal = vect[index][num_literal.len()..].to_string();
        vect.insert(index, num_literal);
        vect[index + 1] = "*".to_string();
        vect.insert(index + 2, rest_literal);
        check_reduced_x(vect);
    }
}

fn invert_x(vect: &mut Vec<String>, begin: usize) {

    let mut outloop = false;
    let mut index = 0;

    for (i, v) in vect.iter().enumerate() {
        if i < begin {
            continue ;
        }
        match v.as_str() {
            "*" => {
                outloop = true;
                index = i;
                break ;
            }
            _ => {continue ;}
        }
    }
    if outloop {
        if vect[index - 1].chars().nth(0).unwrap() == 'x' {
            let temp = vect[index + 1].clone();
            vect[index + 1] = vect[index - 1].clone();
            vect[index - 1] = temp;
        }
        invert_x(vect, index + 1);
    }
}

fn contract_power(vect: &mut Vec<String>) {

    let mut outloop = false;
    let mut index: usize = 0;

    for (i, v) in vect.iter().enumerate() {
        match v.as_str() {
            "^" => {
                if vect[i - 1] == "x" {
                    outloop = true;
                    index = i;
                    break ;
                }
            }
            _ => {continue;}
        }
    }
    if outloop {
        vect[index - 1] = (vect[index - 1].clone() + &vect[index].clone() + &vect[index + 1].clone()).to_string();
        vect.remove(index);
        vect.remove(index);
    }
}