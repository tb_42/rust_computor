use crate::computorv1::utils;

pub fn pol_values(vect: &Vec<String>) {

    let mut num: f32 = 1f32;
    let mut sign: f32 = 1f32;
    let mut equal: f32 = 1f32;
    let mut rev: f32 = 1f32;
    let mut order: usize;
    let mut nvec: Vec<f32> = vec!(0f32, 0f32, 0f32);

    for (i, v) in vect.iter().enumerate() {
        match v.as_str() {
            "*" => continue,
            "X" | "x" => {
                nvec[1] += num * sign * equal;
                num = 1f32;
            },
            "-" => {
                sign = -1f32;
            },
            "+" => {
                sign = 1f32;
            },
            "=" => {
                equal = -1f32;
                sign = 1f32;
            },
            "(" => {
                if i > 0 && vect[i - 1] == "-" {
                    rev *= -1f32;
                    sign = 1f32;
                }
            }
            ")" => {
                rev = 1f32;
            }
            _ => {
                if v.len() > 2 && (&v[0..2] == "X^" || &v[0..2] == "x^") {
                    match &v[2..].parse::<usize>() {
                        Ok(s) => order = *s,
                        Err(_e) => continue,
                    }
                    while nvec.len() - 1 < order.into() {
                        nvec.push(0f32);
                    }
                    nvec[order] += num * sign * equal * rev;
                    num = 1f32;
                } else {
                    match v.parse::<f32>() {
                        Ok(s) => {
                            num = s;
                            if (i < vect.len() - 1 && vect[i + 1] != "*")
                            || i == vect.len() - 1 {
                                nvec[0] += num * sign * equal * rev;
                                num = 1f32;
                            }
                        },
                        Err(_e) => continue,
                    }
                }
            }
        }
    }
    while nvec.len() >= 1 && nvec[nvec.len() - 1] == 0f32 {
        nvec.pop();
    }
    print_reduced_form(&nvec);
}

fn print_reduced_form(vect: &[f32]) {

    let mut degree = (vect.len() - 1) as i8;
    let mut first: bool = true;
    if vect.len() == 0{
        degree = -1;
    }
    print!("Reduced form: ");
    for (i, v) in vect.iter().enumerate() {
        if i == 1 && *v > 0f32 && !first {
            if *v != 1f32 {
                print!(" + {} * X", v);
            } else {
                print!(" + X");
            }
        } else if i == 1 && *v < 0f32 && !first {
            if *v != -1f32 {
                print!(" - {} * X", v * -1f32);
            } else {
                print!(" + X");
            }
        } else if i != 0 && *v > 0f32 && !first {
            if *v != 1f32 {
                print!(" + {} * X^{}", v, i);
            } else {
                print!(" + X^{}", i);
            }
        } else if i != 0 && *v < 0f32 && !first {
            if *v != -1f32 {
                print!(" - {} * X^{}", v * -1f32, i);
            } else {
                print!(" - X^{}", i);
            }
        } else if i == 1 && *v > 0f32 && first {
            if *v != 1f32 {
                print!("{} * X", v);
            } else {
                print!("X");
            }
            first = false;
        } else if i == 1 && *v < 0f32 && first {
            if *v != -1f32 {
                print!("- {} * X", v * -1f32);
            } else {
                print!("- X");
            }
            first = false;
        } else if i != 0 && *v > 0f32 && first {
            if *v != 1f32 {
                print!("{} * X^{}", v, i);
            } else {
                print!("X^{}", i);
            }
            first = false;
        } else if i != 0 && *v < 0f32 && first {
            if *v != -1f32 {
                print!("- {} * X^{}", v * -1f32, i);
            } else {
                print!("- X^{}", i);
            }
            first = false;
        } else if *v < 0f32 {
            print!("- {}", v * -1f32);
            first = false;
        } else if *v > 0f32 {
            print!("{}", v);
            first = false;
        }
    }
    if degree == -1 {
        println!("None\nEvery number is a solution")
    } else if degree > 2 {
        println!(" = 0\nPolynomial degree: {}", degree);
        println!("The polynomial degree is stricly greater than 2, I can't solve.");
    } else if degree == 1 {
        println!(" = 0\nPolynomial degree: {}", degree);
        println!("The solution is:");
        if vect[0] != 0f32 {
            if utils::count_digits(vect[0])[1] == 0 && utils::count_digits(vect[1])[1] == 0 
            && utils::count_digits(-vect[0] / vect[1])[1] > 3 {
                let fraction = utils::reduce_fraction(-vect[0] as i32, vect[1] as i32);
                println!("x = {}/{}", fraction[0], fraction[1]);
            } else {
                println!("x = {}", -vect[0] / vect[1]);
            }
        } else {
            println!("0");
        }
    } else if degree == 0 {
        println!(" = 0\nThere is no solution");
    } else if degree == 2 {
        solve_degree_two(&vect);
    }
}

fn solve_degree_two(vect: &[f32]) {
    let delta: f32;

    println!(" = 0\nPolynomial degree: 2");
    println!("Solving the discriminant...\n");
    println!("Δ = b^2 - 4ac where ax^2 + bx + c = 0");
    println!("Here : a = {} b = {} c = {}", vect[2], vect[1], vect[0]);
    delta = vect[1] * vect[1] - 4f32 * vect[0] * vect[2];
    println!("Δ = {}", delta);
    if delta > 0f32 {
        print!("\nDiscriminant is positive : the solutions are: ");
        print!(" x1 : (-b - √Δ) / 2a and ");
        print!("x2 : (-b + √Δ) / 2a\n");
        if utils::count_digits(-vect[1] + utils::square_root(delta) as f32)[1] == 0 && utils::count_digits(2f32 * vect[2])[1] == 0 && 
        utils::count_digits((-vect[1] + utils::square_root(delta) as f32) / (2f32 * vect[2]))[1] > 3 {
            let fraction = utils::reduce_fraction((-vect[1] + utils::square_root(delta) as f32) as i32, (2f32 * vect[2]) as i32);
            println!("x1 = {}/{}", fraction[0], fraction[1]);
        } else {
            println!("x1 = {}", (-vect[1] - utils::square_root(delta) as f32) / (2f32 * vect[2]));
        }
        if utils::count_digits(-vect[1] - utils::square_root(delta) as f32)[1] == 0 && utils::count_digits(2f32 * vect[2])[1] == 0 && 
        utils::count_digits((-vect[1] - utils::square_root(delta) as f32) / (2f32 * vect[2]))[1] > 3 {
            let fraction = utils::reduce_fraction((-vect[1] - utils::square_root(delta) as f32) as i32, (2f32 * vect[2]) as i32);
            println!("x2 = {}/{}", fraction[0], fraction[1]);
        } else {
            println!("x2 = {}", (-vect[1] + utils::square_root(delta) as f32) / (2f32 * vect[2]));
        }
    }
    else if delta == 0f32 {
        println!("\nDiscriminant is null : the only solution is: ");
        println!("x0 : -b / 2a");
        if -vect[1] / 2f32 * vect[2] != 0f32 {
            if utils::count_digits(vect[1])[1] == 0 && utils::count_digits(2f32 * vect[2])[1] == 0 
            && utils::count_digits(-vect[1] / 2f32 * vect[2])[1] > 3 {
                let fraction = utils::reduce_fraction(-vect[1] as i32, (2f32 * vect[2]) as i32);
                println!("x0 = {}/{}", fraction[0], fraction[1]);
            } else {
                println!("{}", -vect[1] / 2f32 * vect[2]);
            }
        }
        else {
            println!("x0 = 0");
        }
    }
    else if delta < 0f32 {
        print!("\nDiscriminant is negative : the complex solutions are: ");
        print!("x1 : (-b + i√Δ) / 2a and ");
        print!("x2 : (-b - i√Δ) / 2a\n");
        if vect[1] != 0f32 {
            println!("x1 = ({} + {}i) / {}", -vect[1], utils::square_root(-delta), 2f32 * vect[2]);
            println!("x2 = ({} - {}i) / {}", -vect[1], utils::square_root(-delta), 2f32 * vect[2]);
        }
        else {
            println!("x1 = {}i / {}", utils::square_root(-delta), 2f32 * vect[2]);
            println!("x2 = -{}i / {}", utils::square_root(-delta), 2f32 * vect[2]);
        }
    }
}

