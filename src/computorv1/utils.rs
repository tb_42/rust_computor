pub fn square_root(x: f32) -> f32 {

    let mut temp: f32 = 0f32;
    let mut sqrt: f32 = x / 2f32;

    while sqrt != temp {
        temp = sqrt;
        sqrt = (x / temp + temp) / 2f32;
    }
    sqrt
}

pub fn count_digits(x: f32) -> Vec<i8> {

    let num_string: String = x.to_string();
    let mut i: i8 = 0;
    let mut j: i8 = 0;
    let mut point: bool = false;
    let mut nvec = Vec::<i8>::new();

    for c in num_string.chars() {
        if c == '.' {
            point = true;
        } else if !point {
            i += 1;
        } else {
            j += 1;
        }
    }
    nvec.push(i);
    nvec.push(j);
    return nvec;
}

pub fn reduce_fraction(x: i32, y: i32) -> Vec<i32> {

    let a;
    let b;

    if x < 0 {
        a = -x;
    } else {
        a = x;
    } if y < 0 {
        b = -y;
    } else {
        b = y;
    }
    let d: i32 = _pgcd(a, b);
    let mut nvec = Vec::new();

    if (x > 0 && y < 0) || (x < 0 && y < 0) {
        nvec.push(-x/d);
        nvec.push(-y/d);
    } else {
        nvec.push(x/d);
        nvec.push(y/d);
    }
    return nvec;
}

pub fn _pgcd(x: i32, y: i32) -> i32 {

    if y == 0 {
        return x
    }
    return _pgcd(y, _modulo(x, y));
}

pub fn _modulo(x: i32, y: i32) -> i32 {

    let mut a = y;

    if x < y {
        return x;
    } loop {
        a += y;
        if a > x {
            a -= y;
            return x - a;
        } else if a == x {
            return 0;
        }
    }
}
