use std::collections::HashMap;
use crate::complex;

pub struct Rational {
    pub value: f64,
}

impl Rational {
    pub fn new(x: f64) -> Rational {
        Rational {
            value: x
        }
    }
}

pub struct Complex {
    pub value: f64,
    pub i_value: f64,
}

impl Complex {
    pub fn new(x: f64, y: f64) -> Complex {
        Complex {
            value: x,
            i_value: y,
        }
    }
}

pub struct Matrix {
    pub value: Vec<Vec<f64>>,
    pub dimension: [i8; 2],
}

impl Matrix {
    pub fn new() -> Matrix {
        Matrix {
            value: Vec::new(),
            dimension: [0, 0],
        }
    }
    pub fn copy(mat: Vec<Vec<f64>>, dim: [i8; 2]) -> Matrix {
        Matrix {
            value: mat.clone(),
            dimension: dim,
        }
    }
}

pub struct Functions {
    pub value: Vec<String>,
    pub var_name: String,
}

impl Functions {
    pub fn new(tab: Vec<String>, name: String) -> Functions {
        Functions {
            value: tab.clone(),
            var_name: name,
        }
    }
}

pub struct MegaStruct {
    pub rationals: HashMap<String, Rational>,
    pub complex: HashMap<String, Complex>,
    pub matrix: HashMap<String, Matrix>,
    pub functions: HashMap<String, Functions>,
    pub sub_matrix: usize,
    pub sub_complex: usize,
}

impl MegaStruct {
    pub fn new() -> MegaStruct {
        MegaStruct {
            rationals: HashMap::new(),
            complex: HashMap::new(),
            matrix: HashMap::new(),
            functions: HashMap::new(),
            sub_matrix: 0,
            sub_complex: 0,
        }
    }
}

pub fn reset_temps(vars: &mut MegaStruct) {

    vars.complex.retain(|k, _| k.chars().nth(0).unwrap() != '_');
    vars.matrix.retain(|k, _| k.chars().nth(0).unwrap() != '_');
    vars.sub_complex = 0;
    vars.sub_matrix = 0;
}

pub fn remove_old(arg: &String, vars: &mut MegaStruct) {

    vars.complex.remove(arg);
    vars.rationals.remove(arg);
    vars.matrix.remove(arg);
    vars.functions.remove(arg);
}

pub fn check_type(arg: &str, vars: &MegaStruct) -> String {
    if vars.rationals.contains_key(arg.to_lowercase().as_str()) {
        "rational".to_string()
    } else if vars.complex.contains_key(arg.to_lowercase().as_str()) {
        "complex".to_string()
    } else if vars.matrix.contains_key(arg.to_lowercase().as_str()) {
        "matrix".to_string()
    } else {
        "none".to_string()
    }
}

pub fn get_type(x: &String, vars: &MegaStruct) -> String {

    match (*x).parse::<f64>() {
        Ok(_s) => {
            "rational".to_string()
        }
        Err(_e) => {
            if complex::im_check(x) {
                return "pure_im".to_string()
            }
            match check_type(x, vars).as_str() {
                "rational" => {
                    "rational".to_string()
                }
                "complex" => {
                    "complex".to_string()
                }
                "matrix" => {
                    "matrix".to_string()
                }
                _ => {
                    "-1".to_string()
                }
            }
        }
    }
}

pub fn display_values(vars: &MegaStruct) {
    println!("COMPLEX :");
    for (key, value) in &vars.complex {
        println!("{}: rational part: {} | imaginary part: {}", key, value.value, value.i_value);
    }
    println!("\nRATIONALS :");
    for (key, value) in &vars.rationals {
        println!("{} = {}", key, value.value);
    }
    println!("\nMATRIXES :");
    for (key, value) in &vars.matrix {
        println!("{} = {:?}", key, value.value);
    }
    println!("\nFUNCTIONS :");
    for (key, value) in &vars.functions {
        print!("{}({}) = ", key, value.var_name);
        for i in &value.value {
            print!("{} ", i);
        }
        println!();
    }
}