use crate::types;

pub fn im_check(num: &String) -> bool {

    if num == "i" {return true;}

    let numlast = &num[0..num.len() - 1];
    let im = if num.chars().last().unwrap() == 'i' {true} else {false};

    match numlast.parse::<f64>() {
        Ok(_s) => {
            if im {
                return true;
            }
            return false;
        }
        Err(_e) => {return false;}
    }
}

pub fn expand(vect: &mut Vec<String>) {

    let mut index = 0;
    let mut outloop = false;

    for (i, v) in vect.iter().enumerate() {
        match v.as_str() {
            "i" => {
                index = i;
                outloop = true;
                break ;
            }
            _ => {continue;}
        }
    }
    if outloop {
        vect[index] = "1i".to_string();
        expand(vect);
    }
}

pub fn assign(arg: &String, s: &String, vars: &mut types::MegaStruct, display_only: bool) {

    let x = &vars.complex[s];
    let value = x.value;
    let i_value = x.i_value;

    if x.i_value == 0f64 {
        println!("{}", x.value);
    } else if x.i_value == 1f64 {
        println!("{} + i", value);
    } else if i_value == -1f64 {
        println!("{} - i", value);
    } else if i_value > 0f64 {
        println!("{} + {}i", value, i_value);
    } else if i_value < 0f64 {
        println!("{} - {}i", value, - i_value);
    }
    if !display_only && i_value != 0f64 {
        vars.complex.insert(
            arg.to_string().to_lowercase(), types::Complex::new(value, i_value),
        );
    } else if !display_only {
        vars.rationals.insert(
            arg.to_string().to_lowercase(), types::Rational::new(value),
        );
    }
}

fn add_new_value(value: f64, i_value: f64, vars: &mut types::MegaStruct) -> String {

    let c_ref = vars.sub_complex.clone();

    vars.complex.insert(
        ("_".to_string() + &vars.sub_complex.to_string()).to_string(),
        types::Complex::new(value, i_value),
    );
    vars.sub_complex += 1;
    return ("_".to_string() + &c_ref.to_string()).to_string();
}

pub fn product_ic(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = types::Complex::new(0f64, x[0..x.len() - 1].parse::<f64>().unwrap());
    let new_y = &vars.complex[y];
    let value = new_x.i_value * new_y.i_value * -1f64;
    let i_value = new_x.i_value * new_y.value;

    return add_new_value(value, i_value, vars);
}

pub fn product_ii(x: &String, y: &String) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let value = new_x * new_y * -1f64;

    return value.to_string();
}

pub fn product_ri(x: &String, y: &String) -> String {

    let new_x = x.parse::<f64>().unwrap();
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let value = new_x * new_y;

    return (value.to_string() + "i").to_string();
}

pub fn product_cc(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {
    
    let new_x = &vars.complex[x];
    let new_y = &vars.complex[y];
    let value = new_x.value * new_y.value - new_x.i_value * new_y.i_value;
    let i_value = new_x.value * new_y.i_value + new_y.value * new_x.i_value;

    return add_new_value(value, i_value, vars);
}

pub fn product_rc(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x.parse::<f64>().unwrap();
    let new_y = &vars.complex[y];
    let value: f64 = new_x * new_y.value;
    let i_value: f64 = new_x * new_y.i_value;

    return add_new_value(value, i_value, vars);
}

pub fn divide_ic(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = &vars.complex[y];
    let value = (new_x * new_y.i_value) / (new_y.value * new_y.value + new_y.i_value * new_y.i_value);
    let i_value = (new_x * new_y.value) / (new_y.value * new_y.value + new_y.i_value * new_y.i_value);

    return add_new_value(value, i_value, vars);
}

pub fn divide_ci(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = &vars.complex[x];
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let value = (new_x.i_value * new_y) / (new_y * new_y);
    let i_value = - (new_x.value * new_y) / (new_y * new_y);

    return add_new_value(value, i_value, vars);
}

pub fn divide_ii(x: &String, y: &String) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let value = new_x / new_y;
    
    return value.to_string();
}

pub fn divide_ri(x: &String, y: &String) -> String {

    let new_x = x.parse::<f64>().unwrap();
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let i_value = - (new_x / new_y);

    return (i_value.to_string() + "i").to_string();
}

pub fn divide_ir(x: &String, y: &String) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = y.parse::<f64>().unwrap();
    let i_value = new_x / new_y;

    return (i_value.to_string() + "i").to_string();
}

pub fn divide_cc(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = &vars.complex[x];
    let new_y = &vars.complex[y];
    let value = (new_x.value * new_y.value + new_x.i_value * new_y.i_value) /
    (new_y.value * new_y.value + new_y.i_value * new_y.i_value);
    let i_value = (new_x.i_value * new_y.value - new_x.value * new_y.i_value) /
    (new_y.value * new_y.value + new_y.i_value * new_y.i_value);

    return add_new_value(value, i_value, vars);
}

pub fn divide_rc(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x.parse::<f64>().unwrap();
    let new_y = &vars.complex[y];
    let value = (new_x * new_y.value) / (new_y.value * new_y.value + new_y.i_value * new_y.i_value);
    let i_value = - (new_x * new_y.i_value) / (new_y.value * new_y.value + new_y.i_value * new_y.i_value);

    return add_new_value(value, i_value, vars);
}

pub fn divide_cr(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = &vars.complex[x];
    let new_y = y.parse::<f64>().unwrap();
    let value = new_x.value / new_y;
    let i_value = new_x.i_value / new_y;

    return add_new_value(value, i_value, vars);
}

pub fn add_ic(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = &vars.complex[y];
    let value = new_y.value;
    let i_value = new_x + new_y.i_value;

    return add_new_value(value, i_value, vars);
}

pub fn add_ii(x: &String, y: &String) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let value = new_x + new_y;

    return (value.to_string() + "i").to_string();
}

pub fn add_ir(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = y.parse::<f64>().unwrap();
    let value = new_y;
    let i_value = new_x;

    return add_new_value(value, i_value, vars);
}

pub fn add_cc(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = &vars.complex[x];
    let new_y = &vars.complex[y];
    let value = new_x.value + new_y.value;
    let i_value = new_x.i_value + new_y.i_value;

    return add_new_value(value, i_value, vars);
}

pub fn add_rc(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x: f64 = x.parse::<f64>().unwrap();
    let new_y = &vars.complex[y];
    let value = new_x + new_y.value;
    let i_value = new_y.i_value;

    return add_new_value(value, i_value, vars);
}

pub fn substract_ic(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = &vars.complex[y];
    let value = - new_y.value;
    let i_value = new_x - new_y.value;

    return add_new_value(value, i_value, vars);
}

pub fn substract_ci(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = &vars.complex[x];
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let value = - new_x.value;
    let i_value = new_x.value - new_y;

    return add_new_value(value, i_value, vars);
}

pub fn substract_ii(x: &String, y: &String) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let value = new_x - new_y;

    return (value.to_string() + "i").to_string();
}

pub fn substract_ir(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x[0..x.len() - 1].parse::<f64>().unwrap();
    let new_y = y.parse::<f64>().unwrap();
    let value = - new_y;
    let i_value = new_x;

    return add_new_value(value, i_value, vars);
}

pub fn substract_ri(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x.parse::<f64>().unwrap();
    let new_y = y[0..y.len() - 1].parse::<f64>().unwrap();
    let value = new_x;
    let i_value = - new_y;

    return add_new_value(value, i_value, vars);
}

pub fn substract_cc(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = &vars.complex[x];
    let new_y = &vars.complex[y];
    let value = new_x.value - new_y.value;
    let i_value = new_x.i_value - new_y.i_value;

    return add_new_value(value, i_value, vars);
}

pub fn substract_rc(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x.parse::<f64>().unwrap();
    let new_y = &vars.complex[y];
    let value = new_x - new_y.value;
    let i_value = - new_y.i_value;

    return add_new_value(value, i_value, vars);
}

pub fn substract_cr(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = &vars.complex[x];
    let new_y = y.parse::<f64>().unwrap();
    let value = new_x.value - new_y;
    let i_value = new_x.i_value;

    return add_new_value(value, i_value, vars);
}