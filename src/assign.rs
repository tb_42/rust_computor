use crate::types;
use crate::complex;
use crate::operations;
use crate::matrix;
use crate::function;

pub fn assign_result(vect: &mut Vec<String>, vars: &mut types::MegaStruct) {

    if vect[1] == "(" {
        function::assign(vect, vars);
        return ;
    }
    if !solve_parenthesis(vect, vars, true) {
        println!("ERROR");
        types::reset_temps(vars);
        return ;
    }
    types::remove_old(&vect[0], vars);
    if types::get_type(&vect[2], vars) == "complex" {
        complex::assign(&vect[0], &vect[2], vars, false);
    } else if types::get_type(&vect[2], vars) == "rational" {
        vars.rationals.insert(
            vect[0].clone().to_lowercase(), types::Rational::new(vect[2].parse::<f64>().unwrap()),
        );
        println!("{}", vect[2].parse::<f64>().unwrap());
    } else if types::get_type(&vect[2], vars) == "matrix" {
        matrix::assign(&vect[0], &vect[2], vars);
    } else {
        println!("ERROR");
        types::reset_temps(vars);
        return ;
    }
    types::reset_temps(vars);
    // for (key, value) in &vars.complex {
    //     println!("complex : {}: {} {}", key, value.value, value.i_value);
    // }
    // for (key, value) in &vars.rationals {
    //     println!("rational : {}: {}", key, value.value);
    // }
    // for (key, value) in &vars.matrix {
    //     println!("{}: {:?}", key, value.value);
    // }
}

pub fn solve_parenthesis(vect: &mut Vec<String>, vars: &mut types::MegaStruct, assignation: bool) -> bool {

    let mut end_pos: usize = 0;
    let mut begin_pos: usize = 0;
    let mut outloop: bool = false;
    let mut end: bool = false;
    let mut j;

    for (i, v) in vect.iter().enumerate() {
        match v.as_str() {
            ")" => {
                end_pos = i;
                j = i;
                while vect[j] != "(" {
                    j -=1;
                }
                begin_pos = j;
                outloop = true;
                break ;
            }
            "\n" => {
                if assignation {
                    outloop = true;
                    begin_pos = 0;
                    end_pos = i;
                    end = true;
                    break ;
                }
            }
            "=" => {
                if !assignation {
                    outloop = true;
                    begin_pos = 0;
                    end_pos = i + 1;
                    end = true;
                    break ;
                }
            }
            _ => {
                continue;
            }
        }
    }
    if outloop {
        if !calculate(vect, begin_pos, end_pos, vars) {return false;}
        if !end {
            if !solve_parenthesis(vect, vars, assignation) {return false;}
        }
    }
    return true;
}

fn calculate(vect: &mut Vec<String>, begin: usize, end: usize, vars: &mut types::MegaStruct)  -> bool {

    let mut index = 0;
    let mut addition: bool = false;
    let mut substraction: bool = false;
    let mut parenthesis: bool = false;

    if !solve_priorities(vect, begin, end, vars) {return false;}
    for (i, v) in vect.iter().enumerate() {
        if i < begin {
            continue ;
        }
        match v.as_str() {
            "-" => {
                substraction = true;
                index = i;
                break ;
            }
            "+" => {
                addition = true;
                index = i;
                break ;
            }
            ")" => {
                parenthesis = true;
                index = i;
                break ;
            }
            _ => {continue;}
        }
    }
    if substraction {
        let res = operations::substract(&vect[index - 1], &vect[index + 1], vars);
        if res != "ERROR" {vect.insert(index - 1, res);} else {return false;}
    } else if addition {
        let res = operations::add(&vect[index - 1], &vect[index + 1], vars);
        if res != "ERROR" {vect.insert(index - 1, res);} else {return false;}
    } if substraction || addition {
        vect.remove(index);
        vect.remove(index);
        vect.remove(index);
        if !calculate(vect, begin, end - 2, vars) {return false;}
    } if parenthesis {
        vect.remove(index);
        let mut j = index;
        while vect[j] != "(" {
            j -=1;
        }
        vect.remove(j);
    }
    return true;
}

fn solve_priorities(vect: &mut Vec<String>, begin: usize, end: usize, vars: &mut types::MegaStruct) -> bool {

    let mut multiply: bool = false;
    let mut divide: bool = false;
    let mut modulo: bool = false;
    let mut m_product: bool = false;
    let mut index: usize = 0;

    if !solve_power(vect, begin, end, vars) {return false;}
    for (i, v) in vect.iter().enumerate() {
        if i < begin {
            continue ;
        } else if i >= end {
            break ;
        }
        match v.as_str() {
            "*" => {
                multiply = true;
                index = i;
                break ;
            }
            "/" => {
                divide = true;
                index = i;
                break ;
            }
            "%" => {
                modulo = true;
                index = i;
                break ;
            }
            "**" => {
                m_product = true;
                index = i;
                break ;
            }
            _ => {continue;}
        }
    }
    if multiply {
        let res = operations::product(&vect[index - 1], &vect[index + 1], vars);
        if res != "ERROR" {vect.insert(index - 1, res);} else {return false;}
    } else if divide {
        let res = operations::divide(&vect[index - 1], &vect[index + 1], vars);
        if res != "ERROR" {vect.insert(index - 1, res);} else {return false;}
    } else if modulo {
        let res = operations::modulo(&vect[index - 1], &vect[index + 1], vars);
        if res != "ERROR" {vect.insert(index - 1, res);} else {return false;}
    } else if m_product {
        let res = operations::m_product(&vect[index - 1], &vect[index + 1], vars);
        if res != "ERROR" {vect.insert(index - 1, res);} else {return false;}
    } if multiply || divide || modulo || m_product {
        vect.remove(index);
        vect.remove(index);
        vect.remove(index);
        if !solve_priorities(vect, begin, end - 2, vars) {return false;}
    }
    return true;
}

fn solve_power(vect: &mut Vec<String>, begin: usize, end: usize, vars: &mut types::MegaStruct) -> bool {

    let mut index: usize = 0;
    let mut power: bool = false;

    for (i, v) in vect.iter().enumerate() {
        if i < begin {
            continue ;
        } else if i >= end {
            break ;
        }
        match v.as_str() {
            "^" => {
                power = true;
                index = i;
                break ;
            }
            _ => {continue;}
        }
    }
    if power {
        let res = operations::power(&vect[index - 1], &vect[index + 1], vars);
        if res != "ERROR" {vect.insert(index - 1, res);} else {return false;}
        vect.remove(index);
        vect.remove(index);
        vect.remove(index);
        if !solve_power(vect, begin, end - 2, vars) {return false;}
    }
    return true;
}