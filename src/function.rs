use crate::types;

pub fn assign(vect: &mut Vec<String>, vars: &mut types::MegaStruct) -> bool {

    let mut new_vec: Vec<String> = Vec::new();

    for i in 5..vect.len() - 1 {
        print!("{} ", vect[i]);
        new_vec.push(vect[i].to_string());
    }
    println!();
    types::remove_old(&vect[0], vars);
    vars.functions.insert(
        vect[0].to_string().to_lowercase(),
        types::Functions::new(new_vec, vect[2].clone()),
    );
    return true;
}

pub fn expand(vect: &mut Vec<String>, vars: &types::MegaStruct, assignation: bool) {

    let mut index = 0;
    let mut outloop: bool = false;
    let mut arg: String = "".to_string();
    let mut var: String = "".to_string();

    for (i, v) in vect.iter().enumerate() {
        if (*v).to_string().chars().all(char::is_alphabetic) && ((assignation && i > 1) || !assignation) {
            arg = (*v).to_string().to_lowercase();
            if vars.functions.contains_key(arg.as_str()) {
                outloop = true;
                index = i;
                var = vect[i + 2].clone();
                break ;
            }
        }
    }
    if outloop {
        let func = &vars.functions[arg.as_str()];

        vect.remove(index);
        vect.remove(index);
        vect.remove(index);
        vect.remove(index);

        vect.insert(index, ")".to_string());
        for v in func.value.iter().rev() {
            if (*v) == func.var_name {
                vect.insert(index, var.clone());
            } else {
                vect.insert(index, (*v).clone());
            }
        }
        vect.insert(index, "(".to_string());
        expand(vect, vars, assignation);
    }
}