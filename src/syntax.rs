use crate::types;
use crate::matrix;
use crate::complex;
use crate::function;

pub fn analysis(vect: &mut Vec<String>, vars: &mut types::MegaStruct, assignation: bool) -> bool {

    let mut equal = false;
    let mut func = false;
    let mut count_parenthesis = [0, 0];
    let mut count_brackets = [0, 0];
    let mut matrix_mode: bool = false;
    let mut complex_mode: bool = false;
    let mut matrix_dimension = [0, 0];

    for (i, v) in vect.iter().enumerate() {
        match v.as_str() {
            "=" => {
                if equal || (assignation && !(i == 1 || func && i == 4)) || vect[i + 1] == "\n" {
                    println!("1");
                    return false;
                }
                equal = true;
            }
            "(" => {
                if assignation && i == 0 {
                    return false;
                } else if i == 1 {
                    func = true;
                } else if func && i < 4 {
                    return false;
                } else if (func && i >= 4) || (!func && i >= 2) {
                    count_parenthesis[0] += 1;
                }
                if vect[i + 1] == "+" || vect[i + 1] == ")"
                || vect[i + 1] == "*" || vect[i + 1] == "/" || vect[i + 1] == "%" {
                    return false;
                }
            }
            ")" => {
                if (assignation && i < 3) || i < 2 {
                    return false;
                } else if i != 3 {
                    count_parenthesis[1] += 1;
                }
                if count_parenthesis[1] > count_parenthesis[0] {
                    return false;
                }
            }
            "+" | "-" => {
                if (assignation && i < 2) || i < 1 || vect[i + 1] == "+" || vect[i + 1] == "-" || vect[i + 1] == ")"
                || vect[i + 1] == "*" || vect[i + 1] == "/" || vect[i + 1] == "%" || vect[i + 1] == "\n" {
                    return false;
                }
            }
            "*" => {
                if (assignation && i < 2) || i < 1 || vect[i + 1] == "+" || vect[i + 1] == ")"
                || vect[i + 1] == "/" || vect[i + 1] == "%" || (vect[i + 1] == "*" && vect[i - 1] == "*") 
                || vect[i + 1] == "\n" {
                    return false;
                }
            }
            "^" => {
                if (assignation && i < 2) || i < 1 || vect[i + 1] == "+" || vect[i + 1] == "-" || vect[i + 1] == ")"
                || vect[i + 1] == "/" || vect[i + 1] == "%" || vect[i + 1] == "*"
                || vect[i + 1] == "\n" {
                    return false;
                }
            }
            "/" | "%" => {
                if (assignation && i < 2) || i < 1 || vect[i + 1] == "+" || vect[i + 1] == "-" || vect[i + 1] == ")" || matrix_mode
                || vect[i + 1] == "/" || vect[i + 1] == "%" || vect[i + 1] == "*" || vect[i + 1] == "\n" {
                    return false;
                }
            }
            "i" => {
                if (assignation && i < 2) || i < 1 || (func && i == 3) || matrix_mode {
                    return false;
                }
                complex_mode = true;
            }
            "[" => {
                if (assignation && i < 2) || i < 1 || vect[i + 1] == "*" || vect[i + 1] == "/" || vect[i + 1] == "+"
                || vect[i + 1] == "%" || vect[i + 1] == "-" || vect[i + 1] == "," || complex_mode {
                    return false;
                } else if !matrix_mode {
                    if vect[i + 1] != "[" {
                        return false;
                    }
                    matrix_mode = true;
                }
                count_brackets[0] += 1;
            }
            "," => {
                if !matrix_mode || vect[i + 1] == "[" || vect[i + 1] == "]" || i < 2 || vect[i + 1] == "*" 
                || vect[i + 1] == "/" || vect[i + 1] == "+" || vect[i + 1] == "%" || vect[i + 1] == "," {
                    return false;
                }
                matrix_dimension[0] += 1;
            }
            "]" => {
                if (assignation && i < 3) || i < 2 || vect[i + 1] == "%" || vect[i + 1] == "/" || vect[i + 1] == "," {
                    return false;
                }
                count_brackets[1] += 1;
                if count_brackets[1] > count_brackets[0] {
                    return false;
                }
                if vect[i - 1] == "]" {
                    matrix_dimension = [0, 0];
                    continue;
                } else if matrix_dimension[1] == 0 {
                    matrix_dimension[1] = matrix_dimension[0];
                } else if matrix_dimension[1] != 0 && matrix_dimension[0] != matrix_dimension[1] {
                    return false;
                }
                matrix_dimension[0] = 0;
            }
            ";" => {
                if vect[i - 1] != "]" && vect[i + 1] != "[" {
                    return false;
                }
            }
            "?" => {
                if assignation || vect[i + 1] != "\n" {
                    println!("2");
                    return false;
                }
            }
            "\n" => {
                continue;
            }
            _ => {
                match (*v).parse::<f64>() {
                    Ok(_s) => {
                        if assignation && i == 0 {
                            println!("5");
                            return false;
                        }
                    } Err(_e) => {
                        if complex::im_check(&vect[i]) {
                            complex_mode = true;
                            continue;
                        } else if !((*v).to_string().chars().all(char::is_alphabetic)) {
                            println!("6");
                            return false;
                        }
                    }
                }
            }
        }
    }
    if equal && count_parenthesis[0] == count_parenthesis[1] 
    && count_brackets[0] == count_brackets[1] {
        if matrix_mode {
            matrix::reduce(vect, vars);
        } else if complex_mode {
            complex::expand(vect);
        }
        function::expand(vect, vars, assignation);
        expand_rationals(vect, vars, assignation);
        min_product_reduce(vect, vars);
        return true;
    } else {
        println!("3");
        return false;
    }
}

pub fn expand_rationals(vect: &mut Vec<String>, vars: &types::MegaStruct, assignation: bool) {

    let mut index = 0;
    let mut outloop: bool = false;
    let mut arg: String = "".to_string();

    for (i, v) in vect.iter().enumerate() {
        if (*v).to_string().chars().all(char::is_alphabetic) && ((assignation && i > 1) || !assignation) {
            arg = (*v).to_string().to_lowercase();
            if vars.rationals.contains_key(arg.as_str()) {
                outloop = true;
                index = i;
                break ;
            }
        }
    }
    if outloop {
        vect[index] = vars.rationals[&arg].value.to_string();
        expand_rationals(vect, vars, assignation);
    }
}

pub fn min_product_reduce(vect: &mut Vec<String>, vars: &types::MegaStruct) {

    let mut min = false;
    let mut product = false;
    let mut index = 0;

    for (i, v) in vect.iter().enumerate() {
        match v.as_str() {
            "-" => {
                if vect[i - 1] == "*" || vect[i - 1] == "/" || vect[i - 1] == "=" || vect[i - 1] == "(" {
                    min = true;
                    index = i;
                    break ;
                }
            }
            "*" => {
                if vect[i + 1] == "*" {
                    product = true;
                    index = i;
                    break ;
                }
            }
            _ => {continue;}
        }
    }
    if min {
        vect[index] = "-1".to_string();
        vect.insert(index + 1, "*".to_string());
        min_product_reduce(vect, vars);
    } else if product {
        vect[index] = "**".to_string();
        vect.remove(index + 1);
        min_product_reduce(vect, vars);
    }
}