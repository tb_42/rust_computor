use crate::types;

pub fn assign(arg: &String, s: &String, vars: &mut types::MegaStruct) {

    let x = &vars.matrix[s];
    let new_matrix = types::Matrix::copy(x.value.clone(), x.dimension);
    vars.matrix.insert(
        arg.to_string().to_lowercase(),
        new_matrix,
    );
    for i in 0..vars.matrix[s].dimension[0] {
        println!("{:?}", vars.matrix[s].value[i as usize]);
    }
}

pub fn reduce(vect: &mut Vec<String>, vars: &mut types::MegaStruct) {

    let mut current_matrix: Vec<Vec<f64>> = Vec::new();
    let mut current_line: Vec<f64> = Vec::new();
    let mut matrix_mode: bool = false;
    let mut index: usize = 0;
    let mut outloop: bool = false;
    let mut matrix_dimension = [0, 0];
    let mut sign = 1f64;

    for (i, v) in vect.iter().enumerate() {
        match v.as_str() {
            "[" => {
                if vect[i + 1] == "[" {
                    matrix_mode = true;
                    index = i;
                    continue;
                }
            }
            "-" => {
                sign = -1f64;
            }
            "," => {
                matrix_dimension[1] += 1;
            }
            ";" => {
                matrix_dimension[0] += 1;
                current_matrix.push(current_line.clone());
                current_line.clear();
            }
            "]" => {
                if vect[i - 1] == "]" {
                    current_matrix.push(current_line.clone());
                    current_line.clear();
                    let mut new_matrix = types::Matrix::new();
                    new_matrix.value = current_matrix.clone();
                    new_matrix.dimension = [current_matrix.len() as i8, current_matrix[0].len() as i8];
                    vars.matrix.insert(
                        ("_".to_string() + &vars.sub_matrix.to_string()).to_string(),
                        new_matrix,
                    );
                    current_matrix.clear();
                    outloop = true;
                    break ;
                }
            }
            _ => {
                match (*v).parse::<f64>() {
                    Ok(s) => {
                        if matrix_mode {
                            current_line.push(s * sign);
                        }
                        sign = 1f64;
                    }
                    Err(_e) => {
                        continue;
                    }
                }
            }
        }
    }
    if outloop {
        vect.insert(index, ("_".to_string() + &vars.sub_matrix.to_string()).to_string());
        vars.sub_matrix += 1;
        index += 1;
        while !(vect[index] == "]" && vect[index + 1] == "]") {
            vect.remove(index);
        }
        vect.remove(index);
        vect.remove(index);
        reduce(vect, vars);
    }
}

pub fn m_product(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let mat_x = &vars.matrix[x];
    let mat_y = &vars.matrix[y];

    if mat_x.dimension[0] != mat_y.dimension[1] || mat_x.dimension[1] != mat_y.dimension[0] {
        return "ERROR".to_string();
    } else {
        let mut new_matrix: Vec<Vec<f64>> = Vec::new();
        let mut current_line: Vec<f64> = Vec::new();
        let mut newval;

        for xline in 0..mat_x.dimension[0] {
            for ycol in 0..mat_y.dimension[1] {
                newval = 0f64;
                for xcol in 0..mat_x.dimension[1] {
                    newval += mat_x.value[xline as usize][xcol as usize] * mat_y.value[xcol as usize][ycol as usize];
                }
                current_line.push(newval);
            }
            new_matrix.push(current_line.clone());
            current_line.clear();
        }
    let mut final_matrix = types::Matrix::new();
    final_matrix.value = new_matrix.clone();
    final_matrix.dimension = [new_matrix.len() as i8, new_matrix[0].len() as i8];
    let result = "_".to_string() + &vars.sub_matrix.to_string().to_string();
    vars.matrix.insert(
        result,
        final_matrix,
    );
    vars.sub_matrix += 1;
    return "_".to_string() + &(&vars.sub_matrix - 1).to_string().to_string();
    }
}

pub fn product_r(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let new_x = x.parse::<f64>().unwrap();
    let mat_y = &vars.matrix[y];
    let mut new_matrix: Vec<Vec<f64>> = Vec::new();
    let mut current_line: Vec<f64> = Vec::new();

    for line in mat_y.value.iter() {
        for col in line.iter() {
            current_line.push(col * new_x);
        }
        new_matrix.push(current_line.clone());
        current_line.clear();
    }
    let mut final_matrix = types::Matrix::new();
    final_matrix.value = new_matrix.clone();
    final_matrix.dimension = [new_matrix.len() as i8, new_matrix[0].len() as i8];
    let result = "_".to_string() + &vars.sub_matrix.to_string().to_string();
    vars.matrix.insert(
        result,
        final_matrix,
    );
    vars.sub_matrix += 1;
    return "_".to_string() + &(&vars.sub_matrix - 1).to_string().to_string();
}

pub fn add(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let mat_x = &vars.matrix[x];
    let mat_y = &vars.matrix[y];
    let mut new_matrix: Vec<Vec<f64>> = Vec::new();
    let mut current_line: Vec<f64> = Vec::new();

    if mat_x.dimension != mat_y.dimension {
        return "ERROR".to_string();
    }
    for xline in 0..mat_x.dimension[0] {
        for xcol in 0..mat_x.dimension[1] {
            current_line.push(mat_x.value[xline as usize][xcol as usize] + mat_y.value[xline as usize][xcol as usize]);
        }
        new_matrix.push(current_line.clone());
        current_line.clear();
    }
    let mut final_matrix = types::Matrix::new();
    final_matrix.value = new_matrix.clone();
    final_matrix.dimension = [new_matrix.len() as i8, new_matrix[0].len() as i8];
    let result = "_".to_string() + &vars.sub_matrix.to_string().to_string();
    vars.matrix.insert(
        result,
        final_matrix,
    );
    vars.sub_matrix += 1;
    return "_".to_string() + &(&vars.sub_matrix - 1).to_string().to_string();
}

pub fn substract(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let mat_x = &vars.matrix[x];
    let mat_y = &vars.matrix[y];
    let mut new_matrix: Vec<Vec<f64>> = Vec::new();
    let mut current_line: Vec<f64> = Vec::new();

    if mat_x.dimension != mat_y.dimension {
        return "ERROR".to_string();
    }
    for xline in 0..mat_x.dimension[0] {
        for xcol in 0..mat_x.dimension[1] {
            current_line.push(mat_x.value[xline as usize][xcol as usize] - mat_y.value[xline as usize][xcol as usize]);
        }
        new_matrix.push(current_line.clone());
        current_line.clear();
    }
    let mut final_matrix = types::Matrix::new();
    final_matrix.value = new_matrix.clone();
    final_matrix.dimension = [new_matrix.len() as i8, new_matrix[0].len() as i8];
    let result = "_".to_string() + &vars.sub_matrix.to_string().to_string();
    vars.matrix.insert(
        result,
        final_matrix,
    );
    vars.sub_matrix += 1;
    return "_".to_string() + &(&vars.sub_matrix - 1).to_string().to_string();
}

pub fn product(x: &String, y: &String, vars: &mut types::MegaStruct) -> String {

    let mat_x = &vars.matrix[x];
    let mat_y = &vars.matrix[y];
    let mut new_matrix: Vec<Vec<f64>> = Vec::new();
    let mut current_line: Vec<f64> = Vec::new();

    if mat_x.dimension != mat_y.dimension {
        return "ERROR".to_string();
    }
    for xline in 0..mat_x.dimension[0] {
        for xcol in 0..mat_x.dimension[1] {
            current_line.push(mat_x.value[xline as usize][xcol as usize] * mat_y.value[xline as usize][xcol as usize]);
        }
        new_matrix.push(current_line.clone());
        current_line.clear();
    }
    let mut final_matrix = types::Matrix::new();
    final_matrix.value = new_matrix.clone();
    final_matrix.dimension = [new_matrix.len() as i8, new_matrix[0].len() as i8];
    let result = "_".to_string() + &vars.sub_matrix.to_string().to_string();
    vars.matrix.insert(
        result,
        final_matrix,
    );
    vars.sub_matrix += 1;
    return "_".to_string() + &(&vars.sub_matrix - 1).to_string().to_string();
}