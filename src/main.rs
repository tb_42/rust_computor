use std::io::{self, Write};
mod parser;
mod types;
mod syntax;
mod assign;
mod matrix;
mod complex;
mod operations;
mod function;
mod calculate;
mod computorv1 {
    pub mod parser;
    pub mod solver;
    pub mod utils;
}

fn main() {

    let mut s = String::from("");
    let mut vars = types::MegaStruct::new();

    loop {
        print!("> ");
        let _ = io::stdout().flush();
        match io::stdin().read_line(&mut s) {
            Ok(_s) => {
                if s == "exit\n" {
                    println!("Goodbye !");
                    return ;
                } else if s == "display\n" {
                    types::display_values(&vars);
                } else {
                    let len = s.len();
                    parser::parse(&s, &mut vars, &s[len-2..len-1] != "?");
                }
            }
            Err(e) => {
                println!("{}", e);
            }
        }
        s.clear();
    }
}
