use crate::syntax;
use crate::types;
use crate::assign;
use crate::calculate;
use crate::computorv1;

pub fn parse(text: &String, vars: &mut types::MegaStruct, assignation: bool) {

    let mut result = Vec::new();
    let mut last = 0;
    let mut newvec = Vec::new();

    for (index, matched) in text.match_indices(|c: char| !(c.is_alphanumeric() || c == '.')) {
        if last != index {
            result.push(&text[last..index]);
        }
        result.push(matched);
        last = index + matched.len();
    }
    if last < text.len() {
        result.push(&text[last..]);
    }
    result.retain(|x| *x != " ");
    for i in result {
        newvec.push(i.to_string());
    }
    computorv1::parser::check_reduced_x(&mut newvec);
    if assignation {
        let is_ok: bool = syntax::analysis(&mut newvec, vars, assignation);
        if !is_ok {
            println!("Syntax Error");
            return ;
        } else {
            assign::assign_result(&mut newvec, vars);
        }
    } else if newvec[newvec.len() - 2] == "?" && newvec[newvec.len() - 3] == "=" {
        let is_ok: bool = syntax::analysis(&mut newvec, vars, assignation);
        if !is_ok {
            println!("Syntax Error");
            return ;
        } else {
            calculate::calculate_result(&mut newvec, vars);
        }
    } else {
        computorv1::parser::parser(text, vars);
    }
}
