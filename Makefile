SRCS=	src/main.rs \
		src/assign.rs \
		src/complex.rs \
		src/matrix.rs \
		src/parser.rs \
		src/syntax.rs \
		src/types.rs \
		src/operations.rs \
		src/function.rs \
		src/calculate.rs \
		src/computorv1/parser.rs \
		src/computorv1/solver.rs \
		src/computorv1/utils.rs \

NAME=	computorv2

$(NAME):	$(SRCS)
			cargo build --release
			mv ./target/release/computorv2 $(NAME)

all:	$(NAME)

clean:
			cargo clean

fclean:		clean
			rm -f $(NAME)

re:			fclean all